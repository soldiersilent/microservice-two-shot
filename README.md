# Wardrobify

Team:

* Person 1 - Ramon - Shoes Microservice



* Person 2 - Murphey - Hats microservice



## Design

## Shoes microservice

The goal was to keep the wardrobe api data seperated and clean from the Shoes model data. In an attempt to do this, I built a BinVO that gets updated via a poller that pulls from the wardrobe api. If a bin gets created within that api. Shoes API will receive the information and attach as needed to the BinVO. The shoe model then uses that information via a foreignkey to establish many shoes to 1 BinVO.

## Hats microservice

My LocationVO model integrates with the wardrobe microservice via a poller that copies the data from the wardrobe Location model. My other model is the Hat, which contains the required parameters for creating Hats.
